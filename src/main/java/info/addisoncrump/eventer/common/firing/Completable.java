/*
 * Eventer-API: The API for the Eventer event library.
 * Copyright (C) 2018 Addison Crump
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.addisoncrump.eventer.common.firing;

import org.jetbrains.annotations.Contract;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;

public interface Completable extends Runnable {
    @CheckReturnValue
    @Nonnull
    @Contract(pure = true)
    Runnable getOnCompletion();

    @Contract(value = "null -> fail", mutates = "this")
    void setOnCompletion(@Nonnull Runnable onCompletion);
}
