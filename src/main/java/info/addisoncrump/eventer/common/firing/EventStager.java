/*
 * Eventer-API: The API for the Eventer event library.
 * Copyright (C) 2018 Addison Crump
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.addisoncrump.eventer.common.firing;

import info.addisoncrump.eventer.common.bus.Bus;
import info.addisoncrump.eventer.common.handler.Handler;
import org.jetbrains.annotations.Contract;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

@NotThreadSafe
public interface EventStager<E> {
    void fire();

    @CheckReturnValue
    @Contract(value = "null -> fail; !null -> !null", mutates = "this")
    On<E> forEach(Consumer<Handler<E, ?>> consumer);

    @CheckReturnValue
    @Contract(value = "null -> fail; !null -> this", mutates = "this, param")
    EventStager<E> before(Completable before);

    @CheckReturnValue
    @Contract(value = "null -> fail; !null -> this", mutates = "this, param")
    EventStager<E> after(Completable after);

    @CheckReturnValue
    @Nonnull
    @Contract(pure = true)
    Bus getBus();

    @CheckReturnValue
    @Nonnull
    @Contract(pure = true)
    E getEvent();

    @CheckReturnValue
    @Nonnull
    @Contract(pure = true)
    Stream<Completable> getPrefiring();

    @CheckReturnValue
    @Contract(pure = true)
    int getPrefiringSize();

    @CheckReturnValue
    @Nonnull
    @Contract(pure = true)
    Stream<Consumer<Handler<E, ?>>> getHandling();

    @CheckReturnValue
    @Contract(pure = true)
    int getHandlingSize();

    @CheckReturnValue
    @Nonnull
    @Contract(pure = true)
    Stream<Completable> getPostfiring();

    @CheckReturnValue
    @Contract(pure = true)
    int getPostfiringSize();

    interface On<E> {
        @CheckReturnValue
        @Contract(value = "null -> fail; !null -> this", mutates = "this, param")
        On<E> onlyIf(Predicate<Handler<E, ?>> predicate);

        @CheckReturnValue
        @Contract(value = "null -> fail; !null -> this", mutates = "this, param")
        <T> On<E> then(@Nonnull EventStager<T> stager);

        @CheckReturnValue
        @Nonnull
        @Contract(pure = true)
        EventStager<E> and();
    }
}
