/*
 * Eventer-API: The API for the Eventer event library.
 * Copyright (C) 2018 Addison Crump
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.addisoncrump.eventer.common.util;

/**
 * Both marks a class as uninstantiable and prevents instantiation with an {@link UnsupportedOperationException} within
 * the constructor.
 *
 * @author Addison Crump
 * @version 1.0
 * @since 1.0
 */
public abstract class Uninstantiable {
    /**
     * Main constructor of the Uninstantiable type. Any attempts to use this will result in an {@link
     * UnsupportedOperationException}.
     *
     * @since 1.0
     */
    protected Uninstantiable() {
        throw new UnsupportedOperationException("This class cannot be instantiated.");
    }
}
