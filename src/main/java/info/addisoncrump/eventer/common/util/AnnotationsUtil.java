/*
 * Eventer-API: The API for the Eventer event library.
 * Copyright (C) 2018 Addison Crump
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.addisoncrump.eventer.common.util;

import org.jetbrains.annotations.NotNull;

import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeKind;

/**
 * Some general utilities for annotations processing.
 *
 * @author Addison Crump
 * @version 1.0
 * @since 1.0
 */
public class AnnotationsUtil extends Uninstantiable {
    /**
     * Ensures that a target element is visible all the way up (we can call it from anywhere without {@link
     * java.lang.reflect.Method#setAccessible(boolean)}).
     *
     * @param element The element to ensure upwards visibility for.
     * @return visible If the element is upwards visible.
     * @since 1.0
     */
    public static boolean checkUpwardsVisibility(final @NotNull Element element) {
        if (element.asType().getKind() == TypeKind.PACKAGE) {
            return true;
        } else if (!element.getModifiers().contains(Modifier.PUBLIC)) {
            return false;
        } else {
            return checkUpwardsVisibility(element.getEnclosingElement());
        }
    }
}
