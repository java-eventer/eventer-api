/*
 * Eventer-API: The API for the Eventer event library.
 * Copyright (C) 2018 Addison Crump
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.addisoncrump.eventer.common.annotation.processor;

import info.addisoncrump.eventer.common.annotation.EventListener;
import info.addisoncrump.eventer.common.util.AnnotationsUtil;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.type.*;
import javax.lang.model.util.SimpleElementVisitor8;
import javax.lang.model.util.SimpleTypeVisitor8;
import javax.tools.Diagnostic;
import java.util.List;
import java.util.Set;

/**
 * Processor for the {@link EventListener} listener, which ensures that all event listening methods are reachable and invokable.
 *
 * @author Addison Crump
 * @version 1.0
 * @since 1.0
 */
@SupportedAnnotationTypes("info.addisoncrump.eventer.common.annotation.EventListener")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class EventListenerProcessor extends AbstractProcessor {
    private final TypeVisitor<Void, ExecutableElement> ensureNotVoidOrNull =
            new SimpleTypeVisitor8<Void, ExecutableElement>() {
                @Override
                public Void visitNull(final NullType t, final ExecutableElement element) {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String
                            .format("%s#%s: The type specified by the EventListener declaration must not be null.",
                                    element.getEnclosingElement().toString(), element.toString()));
                    return null;
                }

                @Override
                public Void visitDeclared(final DeclaredType t, final ExecutableElement element) {
                    if (t.toString().equals(Void.class.getTypeName())) {
                        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, String.format(
                                "%s#%s method has the default uninstantiable event type Void specified. Is this a mistake?",
                                element.getEnclosingElement().toString(), element.toString()));
                    }
                    return null;
                }
            };
    private final ElementVisitor<Void, TypeMirror> signatureVerificationMirror =
            new SimpleElementVisitor8<Void, TypeMirror>() {
                @Override
                public Void visitExecutable(final ExecutableElement e, final TypeMirror mirror) {
                    List<? extends VariableElement> parameters = e.getParameters();
                    if (parameters.isEmpty()) {
                        mirror.accept(ensureNotVoidOrNull, e);
                    } else {
                        if (parameters.size() == 1) {
                            if (!mirror.toString().equals(Void.class.getTypeName()) && !parameters.get(0).asType()
                                    .toString().equals(mirror.toString())) {
                                processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String
                                        .format("%s#%s: Event listener must accept their specified event type.",
                                                e.getEnclosingElement().toString(), e.toString()));
                            }
                        } else {
                            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String
                                    .format("%s#%s: Event listener must define one argument or none.",
                                            e.getEnclosingElement().toString(), e.toString()));
                        }
                    }
                    if (e.getModifiers().stream().anyMatch(modifier -> {
                        switch (modifier) {
                            case ABSTRACT:
                                return true;
                            case STATIC:
                                return true;
                            default:
                                return false;
                        }
                    })) {
                        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String
                                .format("%s#%s: Event listener must be non-static, implemented methods.",
                                        e.getEnclosingElement().toString(), e.toString()));
                    }
                    if (!AnnotationsUtil.checkUpwardsVisibility(e)) {
                        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String
                                .format("%s#%s: Event listening methods must be publicly accessible; check upwards publicity.",
                                        e.getEnclosingElement().toString(), e.toString()));
                    }
                    return null;
                }
            };

    @Override
    public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv) {
        annotations.forEach(type -> roundEnv.getElementsAnnotatedWith(type)
                .forEach(element -> element.accept(signatureVerificationMirror, getAnnotationTypeMirror(element))));
        return true;
    }

    private TypeMirror getAnnotationTypeMirror(final Element element) {
        try {
            element.getAnnotation(EventListener.class).value();
        } catch (MirroredTypeException mte) {
            return mte.getTypeMirror();
        }
        return null; // idk, it might happen
    }
}
